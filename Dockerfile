FROM node:12-alpine AS builder

WORKDIR /app
COPY . .

RUN npm install
RUN npm run build && npm run bundle

FROM alpine:3.7

WORKDIR /app
COPY --from=builder /app/ts-binary-alpine ./main

CMD ./main